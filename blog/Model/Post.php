<?php
class Post extends AppModel {

    // validation rules
    public $validate = array(
        'title' => array(
            'rule' => 'notEmpty'
        ),
        'body' => array(
            'rule' => 'notEmpty'
        )
    );

    public function getAllPosts() {
        $retVal = $this->find('all', array(
                'fields' => array(
                    'id',
                    'title'
                )
            ));
        return $retVal;
    }

    public function getSinglePost($id = null) {
        $retVal = $this->find('first', array(
                'conditions' => array(
                    'id' => $id
                )
            ));
        return $retVal;
    }

    public function addPost($postData) {
        return $this->save($postData);
    }

    public function editPost($postData) {
        return $this->save($postData);
    }
}