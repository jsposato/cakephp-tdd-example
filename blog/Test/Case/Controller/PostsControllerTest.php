<?php
    App::uses('Controller', 'AppController');
class PostsControllerTest extends ControllerTestCase {
    public $fixtures = array('app.post');

    /**
     * testIndex
     *
     * Test to make sure index returns an array and that expected values
     * are in the array
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testIndex() {
        $this->testAction('/posts/index');
        $this->assertInternalType('array', $this->vars['posts']);
        $expected = array(
            array(
                'id' => 2,
                'title' => 'A title once again'
            )
        );
        $result = Hash::extract($this->vars['posts'], '{n}.Post[id=2]');
        $this->assertEquals($expected, $result);
    }

    /**
     * testView
     *
     * Test that the view returns an array and that the expected post is
     * in the array
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testView() {
        $this->testAction('/posts/view/3');
        $this->assertInternalType('array', $this->vars['post']);
        $expected = array(
            'Post' => array(
                'id' => 3,
                'title' => 'Title strikes back',
                'body' => 'This is really exciting! Not.',
                'created' => '2012-07-04 10:43:23',
                'updated' => '2012-07-04 10:45:31'
            )
        );
        $this->assertEquals($expected, $this->vars['post']);
    }

    /**
     * testAddViaMock
     *
     * test the add function mocking the controller
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testAddViaMock() {
        $postData = array(
            'Post' => array(
                'title' => 'New Post Title',
                'body' => 'TDD FTW'
            )
        );

        // run the test action to add a post
        $this->testAction('/posts/add/', array(
                'data' => $postData,
                'method' => 'post'
            )
        );

        // post should save and we should be redirected
        $this->assertContains('blog/posts', $this->headers['Location']);

        // We should now have 4 posts
        $this->assertEquals(4, $this->controller->Post->find('count'));
    }

    /**
     * testAddViaMockWithBadData
     *
     * test the add function with bad data mocking the controller
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testAddViaMockWithBadData() {
        // set bad data
        $postBadData = array(
            'Post' => array(
                'title' => '',
                'body' => 'TDD FTW!'
            )
        );

        // run test action to add post with bad data
        $this->testAction('/posts/add', array(
                'data' => $postBadData,
                'method' => 'post'
            )
        );

        // should have validation errors, and we should still only have 3 posts
        $this->assertTrue(!empty($this->controller->Post->validationErrors));
        $this->assertContains('This field cannot be left blank', $this->controller->Post->validationErrors['title']);
        $this->assertEquals(3, $this->controller->Post->find('count'));
        $this->assertTrue(empty($this->headers));
    }
}