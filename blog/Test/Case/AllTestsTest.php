<?php
/*
 * This test group will run all tests.
 *
 * @package       Cake.Test.Case
 */
class AllTestsTest extends PHPUnit_Framework_TestSuite {

/**
 * Suite define the tests for this suite
 *
 * @return void
 */
 
    public static function suite() {

        $path = APP . 'Test' . DS . 'Case' . DS;

        $suite = new CakeTestSuite('All tests');
        $suite->addTestDirectory($path . 'Model' . DS);
        $suite->addTestDirectory($path . 'Controller' . DS);
        return $suite;

    }
}
?>
