<?php
App::uses('Post','Model');

class PostTest extends CakeTestCase {
    public $fixtures = array(
        'app.post'
    );

    public function setUp() {
        parent::setUp();
        $this->Post = ClassRegistry::init('Post');
    }

    /**
     * testGetAllPosts
     *
     * Test to make sure that the posts we expect are in the model
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testGetAllPosts() {
        $result = $this->Post->getAllPosts();
        $expected = array(
            array('Post' => array('id' => 1, 'title' => 'The title')),
            array('Post' => array('id' => 2, 'title' => 'A title once again')),
            array('Post' => array('id' => 3, 'title' => 'Title strikes back'))
        );

        $this->assertEquals($expected, $result);
    }

    /**
     * testGetSinglePost
     *
     * Test to make sure retrieving a single post works
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testGetSinglePost() {
        $result = $this->Post->getSinglePost(2);
        $expected = array(
            'Post' => array(
                'id' => 2,
                'title' => 'A title once again',
                'body' => 'And the post body follows.',
                'created' => '2012-07-04 10:41:23',
                'updated' => '2012-07-04 10:43:31'
            )
        );

        $this->assertEquals($expected, $result);
    }

    /**
     * testAddPost
     *
     * Make sure adding a post happens correctly
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testAddPost() {
        $postData = array(
            'title' => 'Test Post Title',
            'body' => 'We love TDD. Yeah!',
            'created' => '2012-07-04 10:41:23',
            'updated' => '2012-07-04 10:43:31'
        );

        $numRecordsBefore = $this->Post->find('count');
        $result = $this->Post->addPost($postData);
        $numRecordsAfter = $this->Post->find('count');

        $expected = array(
            'Post' => array(
                'id' => 4,
                'title' => 'Test Post Title',
                'body' => 'We love TDD. Yeah!',
                'created' => '2012-07-04 10:41:23',
                'updated' => '2012-07-04 10:43:31'
            )
        );

        $this->assertEquals(4, $numRecordsAfter);
        $this->assertTrue($numRecordsBefore != $numRecordsAfter);
        $this->assertEquals($expected,$result);
    }

    /**
     * testEditPost
     *
     * Make sure editing a post happens correctly
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testEditPost() {
        $this->Post->id = 3;
        $postData = array(
            'title' => 'Test Post Title.  Updated',
            'body' => 'We love TDD.  Yeah!  Yeah!',
            'created' => '2012-07-04 10:43:23',
            'updated' => '2012-07-04 10:49:51'
        );

        $recordBeforeEdit = $this->Post->read();
        $numRecordsBefore = $this->Post->find('count');
        $result = $this->Post->editPost($postData);
        $numRecordsAfter = $this->Post->find('count');

        $expected = array(
            'Post' => array(
                'id' => 3,
                'title' => 'Test Post Title.  Updated',
                'body' => 'We love TDD.  Yeah!  Yeah!',
                'created' => '2012-07-04 10:43:23',
                'updated' => '2012-07-04 10:49:51'
            )
        );

        $this->assertEquals($expected, $result);
        $this->assertTrue($numRecordsBefore == $numRecordsAfter);
        $this->assertNotEquals($recordBeforeEdit, $result);

        $recordCompare = array_diff($recordBeforeEdit['Post'], $result['Post']);
        $expectedArrayDiffResult = array(
            'title' => 'Title strikes back',
            'body' => 'This is really exciting! Not.',
            'updated' => '2012-07-04 10:45:31'
        );

        $this->assertEquals($expectedArrayDiffResult, $recordCompare);
    }

    /**
     * testTitleValidation
     *
     * Test that a post without a title cannot be inserted
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testTitleValidation() {
        $postData = array(
            'title' => '',
            'body' => 'Oh no, this post has an empty title'
        );
        $result = $this->Post->addPost($postData);
        $invalidFields = $this->Post->invalidFields();

        $this->assertFalse($result);
        $this->assertContains('This field cannot be left blank', $invalidFields['title']);
    }

    /**
     * testBodyValidation
     *
     * Test that a post without a body cannot be inserted
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testBodyValidation() {
        $postData = array(
            'title' => 'No body in this post?  Impossible',
            'body' => ''
        );
        $result = $this->Post->addPost($postData);
        $invalidFields = $this->Post->invalidFields();

        $this->assertFalse($result);
        $this->assertContains('This field cannot be left blank', $invalidFields['body']);
    }

    /**
     * testTitleAndBodyValidation
     *
     * Test to be sure that if required fields are present, the save succeeds
     *
     *
     * @author  jsposato
     * @version 1.0
     */
    public function testTitleAndBodyValidation() {
        $postData = array(
            'title' => 'Title',
            'body' => '... and body'
        );
        $result = $this->Post->addPost($postData);
        $invalidFields = $this->Post->invalidFields();

        $this->assertFalse(empty($result));
        $this->assertTrue(empty($invalidFields));
    }
}